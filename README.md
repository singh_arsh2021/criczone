INTRODUCTION:
This repository is of cricket website.

DESCRIPTION:
This website includes basic information about the cricket. It has 5 tabs in its home page namely Home, Gallery, Video, Team Rankings and Feedback

*Home: Home page basically includes cricket news and some quick links of the website.
*Gallery: This includes the image gallery of various cricket matches.
*Video: This includes two videos of cricket matches.
*Team Rankings: This shows rankings of ODI and Test batsman.
*Feedback: This page shows the feedback form which may be suggestion, bug or modification.

REQUIREMENTS:
To run this project, user needs to download .html file and run on any web browser.
To make any changes or for developing purpose, Visual Studio Code needs to be installed on the machine.
